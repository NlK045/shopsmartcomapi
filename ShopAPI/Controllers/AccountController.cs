﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using ShopAPI.Models;

namespace ShopAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();

        public AccountController() {}

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public  UserInfoViewModel GetUserInfo()
        {
            
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
            if(currentUser == null) return new UserInfoViewModel { };
            IList<string> roles = new List<string> { "Роль не определена" };
            roles = UserManager.GetRoles(currentUser.Id);
            ApplicationDbContext userContext = new ApplicationDbContext();
            userContext.Users.ToList();
            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                Address = currentUser.Address,
                Code = currentUser.Code,
                Discount = currentUser.Discount,
                Id = currentUser.Id,
                Role = roles.LastOrDefault()
            };
        }

        // GET api/Account/UserList
        [Authorize(Roles = "Admin")]
        [Route("UserList")]
        public async Task<IHttpActionResult> GetUserList()
        {
            ApplicationDbContext userContext = new ApplicationDbContext();
            List<UserInfoViewModel> userAll = new List<UserInfoViewModel>();
            foreach (var el in userContext.Users.ToList())
            {
                var userRoles = await UserManager.GetRolesAsync(el.Id);
                foreach (var userR in userRoles)
                {
                    userAll.Add(new UserInfoViewModel { Email = el.Email, Address = el.Address, Code = el.Code, Discount = el.Discount, Id = el.Id, Role = userR });
                }
            }
            return Ok(userAll);
        }

        //[AllowAnonymous]
        //[Route("Auth")]
        //public async Task<IHttpActionResult> Auth(AuthBindningModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);
        //    if (user == null) return Content(HttpStatusCode.NotFound, "Данного пользователь не существует");

        //    ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //       OAuthDefaults.AuthenticationType);
        //    ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //        CookieAuthenticationDefaults.AuthenticationType);

        //    AuthenticationProperties properties = CreateProperties(user.UserName);
        //    AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
        //    context.Validated(ticket);
        //    context.Request.Context.Authentication.SignIn(cookiesIdentity);

        //    return Ok(properties);
        //}

        //PUT api/Account/EditUser
        [Authorize(Roles = "Admin")]
        [Route("EditUser")]
        public async Task<IHttpActionResult> PutEditUser(EditBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationUser user = UserManager.FindById(model.Id);
            if (user == null) return Content(HttpStatusCode.NotFound, String.Format("Пользователь с указанным Id={0} не найден", model.Id));
            if (model.Address != null) user.Address = model.Address;
            if (model.Discount != user.Discount) user.Discount = model.Discount;
            if (model.Email != null)
            {
                user.Email = model.Email;
                user.UserName = model.Email;
            }
            if (model.Role != null)
            {
                var userRoles = await UserManager.GetRolesAsync(user.Id);
                foreach (var el in userRoles)
                {
                    await UserManager.RemoveFromRoleAsync(user.Id, el);
                }
                await UserManager.AddToRoleAsync(user.Id, model.Role);
            }
            IdentityResult result = await UserManager.UpdateAsync(user);


            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(new string[] { "Успешное редактирование аккаунта" });
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        //Delete /Users/DeleteUser/
        [HttpDelete]
        [AllowAnonymous]
        [Route("DeleteUser")]
        public async Task<IHttpActionResult> DeleteUser(DeleteUserModel model)
        {
            if (model.userID == null)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(model.userID);
            List<Order> order = db.Orders.Where(p => p.userId == model.userID).ToList();
            foreach (var or in order)
            {
                db.Orders.Remove(or);
            }
            await db.SaveChangesAsync();
            IdentityResult result =  await UserManager.DeleteAsync(user);

            if (user == null)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (model.Role == null)
            {
                model.Role = "Customer";
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email, Address = model.Address };
           
            Random rnd = new Random();
            user.Code = rnd.Next(1000, 9999).ToString() + "-" + DateTime.Now.Year.ToString();
            user.Discount = model.Discount;
            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            await UserManager.AddToRoleAsync(user.Id, model.Role); 

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Вспомогательные приложения

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // Ошибки ModelState для отправки отсутствуют, поэтому просто возвращается пустой BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("Значение strengthInBits должно нацело делиться на 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }
        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
        #endregion
    }
}
