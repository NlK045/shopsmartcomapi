﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using ShopAPI.Database;
using ShopAPI.Models;

namespace ShopAPI.Controllers
{
    public class CartOrdersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: api/CartOrders
        public async Task<IHttpActionResult> PostCartOrder(CartOrderBindingModels cartOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser user = db.Users.AsEnumerable().FirstOrDefault(p => p.Id == HttpContext.Current.User.Identity.GetUserId());
            OrderItem orderItem = db.OrderItems.AsEnumerable().FirstOrDefault(p => p.id == cartOrder.Orderitem_id);
            Order order = db.Orders.AsEnumerable().FirstOrDefault(p => p.id == cartOrder.Order_id);
            if (orderItem == null) return Content(HttpStatusCode.NotFound, String.Format("Предмет с указанным Id={0} не найден",cartOrder.Orderitem_id));
            if (order == null) return Content(HttpStatusCode.NotFound, String.Format("Заказ с указанным Id={0} не найден",cartOrder.Order_id));

            double fullPrice = orderItem.price * cartOrder.ItemCount;
            double discount = (double)user.Discount / 100;
            double itemPrice = fullPrice - fullPrice * discount;

            CartOrder cartOrders = new CartOrder() { itemCount = cartOrder.ItemCount, itemPrice = (int)itemPrice, order = order , orderItem = orderItem };
            db.CartOrders.Add(cartOrders);
            await db.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/CartOrders/5
        [ResponseType(typeof(CartOrder))]
        public async Task<IHttpActionResult> DeleteCartOrder(int id)
        {
            CartOrder cartOrder = await db.CartOrders.FindAsync(id);
            if (cartOrder == null)
            {
                return NotFound();
            }

            db.CartOrders.Remove(cartOrder);
            await db.SaveChangesAsync();

            return Ok(cartOrder);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CartOrderExists(int id)
        {
            return db.CartOrders.Count(e => e.id == id) > 0;
        }
    }
}