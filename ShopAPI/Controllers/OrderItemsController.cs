﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ShopAPI.Database;
using ShopAPI.Models;

namespace ShopAPI.Controllers
{
    [RoutePrefix("api/OrderItems")]
    public class OrderItemsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationDbContext dbOrderItem = new ApplicationDbContext();

        public OrderItemsController(){ }

        // GET: api/OrderItems
        [AllowAnonymous]
        public IEnumerable<OrderItem> GetOrderItems()
        {
            return db.OrderItems;
        }

        // GET: api/OrderItems/5
        [Authorize(Roles = "Admin,Customer")]
        [ResponseType(typeof(OrderItem))]
        public async Task<IHttpActionResult> GetOrderItem(int id)
        {
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return NotFound();
            }

            return Ok(orderItem);
        }

        // POST: api/OrderItems/Filter
        [Authorize(Roles = "Admin,Customer")]
        [Route("Filter")]
        public IHttpActionResult PostFilter(OrderItemBindingModels orderItem)
        {
            if (orderItem == null) return Content(HttpStatusCode.NotFound, "Не один параметр для фильтрации не указан");
            if (orderItem.name != null)
            {
                var orderItems = db.OrderItems.Where(p => p.name == orderItem.name).ToList();
                if (orderItems == null) return StatusCode(HttpStatusCode.NoContent);
                return Ok(orderItems);
            }
            if (orderItem.category != null)
            {
                var orderItems = db.OrderItems.Where(p => p.category == orderItem.category).ToList();
                if (orderItems == null) return StatusCode(HttpStatusCode.NoContent);
                return Ok(orderItems);
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/OrderItems/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrderItem(int id, OrderItem orderItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            OrderItem orderItemOld = dbOrderItem.OrderItems.AsEnumerable().FirstOrDefault(p => p.id == id);
            if (orderItemOld == null) return Content(HttpStatusCode.NotFound, String.Format("Заказ с указанным Id={0} не найден", id));
            orderItem.id = orderItemOld.id;
            if (orderItem.name == null) orderItem.name = orderItemOld.name;
            if (orderItem.code == null) orderItem.code = orderItemOld.code;
            if (orderItem.category == null) orderItem.category = orderItemOld.category;
            if (orderItem.price == 0) orderItem.price = orderItemOld.price;
            db.Entry(orderItem).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderItems
        [ResponseType(typeof(OrderItem))]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> PostOrderItem(OrderItem orderItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Random rand = new Random();
            string code = "XX-XXXX-YYXX";
            string codeGeneration = "";
            foreach (char codeItem in code)
            {
                char codeLetter = (char)rand.Next('A', 'Z' + 1);
                int codeValue = rand.Next(0, 9);
                if (codeItem == 'X') codeGeneration += codeValue.ToString();
                if (codeItem == '-') codeGeneration += "-";
                if (codeItem == 'Y') codeGeneration += codeLetter.ToString();
            }
            orderItem.code = codeGeneration;
            db.OrderItems.Add(orderItem);
            await db.SaveChangesAsync();

            return Ok(orderItem);
        }

        // DELETE: api/OrderItems/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(OrderItem))]
        public async Task<IHttpActionResult> DeleteOrderItem(int id)
        {
            OrderItem orderItem = await db.OrderItems.FindAsync(id);
            if (orderItem == null)
            {
                return NotFound();
            }

            db.OrderItems.Remove(orderItem);
            await db.SaveChangesAsync();

            return Ok(orderItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderItemExists(int id)
        {
            return db.OrderItems.Count(e => e.id == id) > 0;
        }
    }
}