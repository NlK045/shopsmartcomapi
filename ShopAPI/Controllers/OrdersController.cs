﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ShopAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace ShopAPI.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationDbContext dbU = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/UserOrders
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserOrders")]
        public IHttpActionResult GetUserOrders()
        {
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
            if (currentUser == null) return NotFound();
            List<Order> order = db.Orders.Where(o => o.userId == currentUser.Id).ToList();
            List<OrderListBindingModels> orderInfo = new List<OrderListBindingModels>();
            foreach (var or in order)
            {
                or.user = db.Users.AsEnumerable().FirstOrDefault(p => p.Id == or.userId);
                orderInfo.Add(new OrderListBindingModels
                {
                    id = or.id,
                    address = or.user.Address,
                    code = or.user.Code,
                    discount = or.user.Discount,
                    nameUser = or.user.UserName,
                    orderDate = or.orderDate,
                    shipmentDate = or.shipmentDate,
                    orderNumber = or.orderNumber,
                    status = or.status
                });
            }
            return Ok(orderInfo);

        }

        // GET: api/Orders
        public IHttpActionResult GetOrders()
        {
            List<Order> order = db.Orders.ToList();
            List<OrderListBindingModels> orderInfo = new List<OrderListBindingModels>();
            foreach (var or in order)
            {
                or.user = dbU.Users.AsEnumerable().FirstOrDefault(p => p.Id == or.userId);
                if (or.user != null)
                    orderInfo.Add(new OrderListBindingModels
                    {
                        id = or.id,
                        address = or.user.Address,
                        code = or.user.Code,
                        discount = or.user.Discount,
                        nameUser = or.user.UserName,
                        orderDate = or.orderDate,
                        shipmentDate = or.shipmentDate,
                        orderNumber = or.orderNumber,
                        status = or.status
                    });
            }
            return Ok(orderInfo);
        }

        // POST: api/Filter
        [Authorize(Roles = "Admin,Customer")]
        [Route("Filter")]
        public IHttpActionResult PostFilter(OrderFilterBindingModels orderFilter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Order> order = db.Orders.Where(p => p.status == orderFilter.status).ToList();
            List<OrderListBindingModels> orderInfo = new List<OrderListBindingModels>();
            foreach (var or in order)
            {
                or.user = db.Users.AsEnumerable().FirstOrDefault(p => p.Id == or.userId);
                or.CartOrder = null;
                orderInfo.Add(new OrderListBindingModels
                {
                    id = or.id,
                    address = or.user.Address,
                    code = or.user.Code,
                    discount = or.user.Discount,
                    nameUser = or.user.UserName,
                    orderDate = or.orderDate,
                    shipmentDate = or.shipmentDate,
                    orderNumber = or.orderNumber,
                    status = or.status
                });
            }
            return Ok(orderInfo);
        }

        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> GetOrder(int id)
        {
            OrderDetailBindingModels orderInfo = new OrderDetailBindingModels();
            List<CartOrderModels> cartOrder = new List<CartOrderModels>();
            Order order = await db.Orders.FindAsync(id);
            if (order == null) return Content(HttpStatusCode.NotFound, String.Format("Заказ с указанным Id={0} не найден", id));
            order.user = db.Users.AsEnumerable().FirstOrDefault(p => p.Id == order.userId);
            order.CartOrder = db.CartOrders.AsEnumerable().Where(p => p.orderId == order.id).ToList();
            foreach (var co in order.CartOrder)
            {
                co.orderItem = db.OrderItems.AsEnumerable().FirstOrDefault(p => p.id == co.orderItemId);
                cartOrder.Add(new CartOrderModels
                {
                    id = co.id,
                    count = co.itemCount,
                    nameItem = co.orderItem.name,
                    price = co.itemPrice
                });
            }
            orderInfo = new OrderDetailBindingModels
            {
                id = order.id,
                address = order.user.Address,
                code = order.user.Code,
                discount = order.user.Discount,
                nameUser = order.user.UserName,
                orderDate = order.orderDate,
                shipmentDate = order.shipmentDate,
                orderNumber = order.orderNumber,
                status = order.status,
                cartOrderList = cartOrder
            };
            return Ok(orderInfo);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder(int id, OrderBindingModels order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Order orderOld;
            using (var db = new ApplicationDbContext())
            {
                orderOld = db.Orders.AsEnumerable().FirstOrDefault(p => p.id == id);
            }
            if (orderOld == null) return BadRequest();
            orderOld.status = order.status;
            if (order.shipmentDate != null) orderOld.shipmentDate = order.shipmentDate;
            db.Entry(orderOld).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return Ok(new OrderBindingModels
            {
                status = orderOld.status,
                orderDate = orderOld.orderDate,
                shipmentDate = orderOld.shipmentDate,
                orderNumber = orderOld.orderNumber
            });
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        [Authorize(Roles = "Customer")]
        public async Task<IHttpActionResult> PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser user = db.Users.AsEnumerable().FirstOrDefault(p => p.Id == HttpContext.Current.User.Identity.GetUserId());
            order.user = user;
            order.orderNumber = db.Orders.Count() + 1;
            order.orderDate = DateTime.Now;
            order.shipmentDate = order.orderDate;
            db.Orders.Add(order);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = order.id }, order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> DeleteOrder(int id)
        {
            Order order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }

            db.Orders.Remove(order);
            await db.SaveChangesAsync();

            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            return db.Orders.Count(e => e.id == id) > 0;
        }
    }
}