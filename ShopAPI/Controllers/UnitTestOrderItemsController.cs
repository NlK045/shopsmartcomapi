﻿using ShopAPI.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShopAPI.Controllers
{
    public class UnitTestOrderItemsController : ApiController
    {
        List<OrderItem> orderItems = new List<OrderItem>();

        public UnitTestOrderItemsController() { }

        public UnitTestOrderItemsController(List<OrderItem> orderItems)
        {
            this.orderItems = orderItems;
        }

        // GET: api/OrderItems
        public IEnumerable<OrderItem> GetOrderItems()
        {
            return orderItems;
        }

        // GET: api/AllOrderItems
        public async Task<IEnumerable<OrderItem>> GetAllOrderItems()
        {
            return await Task.FromResult(GetOrderItems());
        }

        // GET: api/OrderItems/5
        public IHttpActionResult GetOrderItems(int id)
        {
            var orderItem = orderItems.FirstOrDefault((p) => p.id == id);
            if (orderItem == null)
            {
                return NotFound();
            }
            return Ok(orderItem);
        }

        // GET: api/OrderItemsAsync
        public async Task<IHttpActionResult> GetOrderItemsAsync(int id)
        {
            return await Task.FromResult(GetOrderItems(id));
        }
    }
}
