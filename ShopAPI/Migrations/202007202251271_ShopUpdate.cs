namespace ShopAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShopUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CartOrders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        itemCount = c.Int(nullable: false),
                        itemPrice = c.Int(nullable: false),
                        order_id = c.Int(),
                        orderItem_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Orders", t => t.order_id)
                .ForeignKey("dbo.OrderItems", t => t.orderItem_id)
                .Index(t => t.order_id)
                .Index(t => t.orderItem_id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        orderDate = c.DateTime(nullable: false),
                        shipmentDate = c.DateTime(nullable: false),
                        orderNumber = c.Int(nullable: false),
                        status = c.String(),
                        user_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.user_Id)
                .Index(t => t.user_Id);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        code = c.String(),
                        category = c.String(),
                        price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            AddColumn("dbo.AspNetUsers", "Discount", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CartOrders", "orderItem_id", "dbo.OrderItems");
            DropForeignKey("dbo.Orders", "user_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.CartOrders", "order_id", "dbo.Orders");
            DropIndex("dbo.Orders", new[] { "user_Id" });
            DropIndex("dbo.CartOrders", new[] { "orderItem_id" });
            DropIndex("dbo.CartOrders", new[] { "order_id" });
            DropColumn("dbo.AspNetUsers", "Code");
            DropColumn("dbo.AspNetUsers", "Discount");
            DropColumn("dbo.AspNetUsers", "Address");
            DropTable("dbo.OrderItems");
            DropTable("dbo.Orders");
            DropTable("dbo.CartOrders");
        }
    }
}
