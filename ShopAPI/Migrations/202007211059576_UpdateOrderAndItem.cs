namespace ShopAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOrderAndItem : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CartOrders", "order_id", "dbo.Orders");
            DropForeignKey("dbo.CartOrders", "orderItem_id", "dbo.OrderItems");
            DropIndex("dbo.CartOrders", new[] { "order_id" });
            DropIndex("dbo.CartOrders", new[] { "orderItem_id" });
            RenameColumn(table: "dbo.CartOrders", name: "order_id", newName: "orderId");
            RenameColumn(table: "dbo.CartOrders", name: "orderItem_id", newName: "orderItemId");
            AddColumn("dbo.Orders", "userId", c => c.Int(nullable: false));
            AlterColumn("dbo.CartOrders", "orderId", c => c.Int(nullable: false));
            AlterColumn("dbo.CartOrders", "orderItemId", c => c.Int(nullable: false));
            CreateIndex("dbo.CartOrders", "orderId");
            CreateIndex("dbo.CartOrders", "orderItemId");
            AddForeignKey("dbo.CartOrders", "orderId", "dbo.Orders", "id", cascadeDelete: true);
            AddForeignKey("dbo.CartOrders", "orderItemId", "dbo.OrderItems", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CartOrders", "orderItemId", "dbo.OrderItems");
            DropForeignKey("dbo.CartOrders", "orderId", "dbo.Orders");
            DropIndex("dbo.CartOrders", new[] { "orderItemId" });
            DropIndex("dbo.CartOrders", new[] { "orderId" });
            AlterColumn("dbo.CartOrders", "orderItemId", c => c.Int());
            AlterColumn("dbo.CartOrders", "orderId", c => c.Int());
            DropColumn("dbo.Orders", "userId");
            RenameColumn(table: "dbo.CartOrders", name: "orderItemId", newName: "orderItem_id");
            RenameColumn(table: "dbo.CartOrders", name: "orderId", newName: "order_id");
            CreateIndex("dbo.CartOrders", "orderItem_id");
            CreateIndex("dbo.CartOrders", "order_id");
            AddForeignKey("dbo.CartOrders", "orderItem_id", "dbo.OrderItems", "id");
            AddForeignKey("dbo.CartOrders", "order_id", "dbo.Orders", "id");
        }
    }
}
