namespace ShopAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOrder : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", new[] { "user_Id" });
            DropColumn("dbo.Orders", "userId");
            RenameColumn(table: "dbo.Orders", name: "user_Id", newName: "userId");
            AlterColumn("dbo.Orders", "userId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Orders", "userId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Orders", new[] { "userId" });
            AlterColumn("dbo.Orders", "userId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Orders", name: "userId", newName: "user_Id");
            AddColumn("dbo.Orders", "userId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "user_Id");
        }
    }
}
