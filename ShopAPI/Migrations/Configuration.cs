namespace ShopAPI.Migrations
{
    using ShopAPI.Helpers;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ShopAPI.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ShopAPI.Models.ApplicationDbContext context)
        {
            IdentityHelper.SeedIdentities(context);
        }
    }
}
