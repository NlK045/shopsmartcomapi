﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ShopAPI.Models
{
    // Модели, используемые в качестве параметров действий AccountController.

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
        [Display(Name = "Код")]
        public string Code { get; set; }
        [Display(Name = "Скидка")]
        public int Discount { get; set; }
        [Required]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        public string Role { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }

    public class AuthBindningModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }
    public class EditBindingModel
    {
        [Required]
        [Display(Name = "Код идентификации")]
        public string Id { get; set; }
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
        [Display(Name = "Скидки")]
        public int Discount { get; set; }
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Display(Name = "Роль")]
        public string Role { get; set; }
    }

    public class DeleteUserModel
    {
        [Required]
        [Display(Name = "Код пользователя")]
        public string userID { get; set; }
    }

        public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Поставщик входа")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Ключ поставщика")]
        public string ProviderKey { get; set; }
    }
}
