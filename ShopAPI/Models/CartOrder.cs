﻿using Newtonsoft.Json;
using ShopAPI.Models;

namespace ShopAPI.Database
{
    [JsonObject(IsReference = true)]
    public class CartOrder
    {
        public int id { get; set; }
        public int orderId { get; set; }
        public int orderItemId { get; set; }
        public int itemCount { get; set; }
        public int itemPrice { get; set; }
        public Order order { get; set; }
        public OrderItem orderItem { get; set; }
    }
}