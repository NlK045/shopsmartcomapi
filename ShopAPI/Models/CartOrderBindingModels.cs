﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopAPI.Models
{
    public class CartOrderBindingModels
    {
        [Required]
        [Display(Name = "Код заказа")]
        public int Order_id { get; set; }
        [Required]
        [Display(Name = "Код элемента заказа")]
        public int Orderitem_id { get; set; }
        [Required]
        [Display(Name = "Количество")]
        public int ItemCount { get; set; } 
    }
}