﻿using Newtonsoft.Json;
using ShopAPI.Database;
using System;
using System.Collections.Generic;

namespace ShopAPI.Models
{
    [JsonObject(IsReference = true)]
    public class Order
    {
        public int id { get; set; }
        public string userId { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime shipmentDate { get; set; }
        public int orderNumber { get; set; }
        public string status { get; set; }
        public virtual ApplicationUser user { get; set; }
        public virtual ICollection<CartOrder> CartOrder { get; set; }
    }
}