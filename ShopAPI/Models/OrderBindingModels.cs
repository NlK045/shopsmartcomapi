﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopAPI.Models
{
    public class OrderBindingModels
    {
        [Required]
        [Display(Name = "Статус")]
        public string status { get; set; }
        [Display(Name = "Номер заказа")]
        public int orderNumber { get; set; }
        [Display(Name = "Дата создания")]
        public DateTime orderDate { get; set; }
        [Display(Name = "Дата доставки")]
        public DateTime shipmentDate { get; set; }


    }

    public class OrderListBindingModels
    {
        [Display(Name = "Код заказа")]
        public int id { get; set; }
        [Display(Name = "Статус")]
        public string status { get; set; }
        [Display(Name = "Номер заказа")]
        public int orderNumber { get; set; }
        [Display(Name = "Дата создания")]
        public DateTime orderDate { get; set; }
        [Display(Name = "Дата доставки")]
        public DateTime shipmentDate { get; set; }
        [Display(Name = "Имя пользователя")]
        public string nameUser { get; set; }
        [Display(Name = "Адрес")]
        public string address { get; set; }
        [Display(Name = "Код пользователя")]
        public string code { get; set; }
        [Display(Name = "Скидка")]
        public int discount { get; set; }
    }
    public class OrderDetailBindingModels
    {
        [Display(Name = "Код заказа")]
        public int id { get; set; }
        [Display(Name = "Статус")]
        public string status { get; set; }
        [Display(Name = "Номер заказа")]
        public int orderNumber { get; set; }
        [Display(Name = "Дата создания")]
        public DateTime orderDate { get; set; }
        [Display(Name = "Дата доставки")]
        public DateTime shipmentDate { get; set; }
        [Display(Name = "Имя пользователя")]
        public string nameUser { get; set; }
        [Display(Name = "Адрес")]
        public string address { get; set; }
        [Display(Name = "Код пользователя")]
        public string code { get; set; }
        [Display(Name = "Скидка")]
        public int discount { get; set; }
        [Display(Name = "Лист элементов заказа")]
        public List<CartOrderModels> cartOrderList { get; set; }
    }

    public class CartOrderModels
    {
        [Display(Name = "Код элемента корзины")]
        public int id { get; set; }
        [Display(Name = "Количество элементов")]
        public int count { get; set; }
        [Display(Name = "Стоимость элементов")]
        public int price { get; set; }
        [Display(Name = "Название элемента")]
        public string nameItem { get; set; }
    }

    public class OrderFilterBindingModels
    {
        [Required]
        [Display(Name = "Статус")]
        public string status { get; set; }
    }
}