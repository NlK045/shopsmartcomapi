﻿using System.ComponentModel.DataAnnotations;

namespace ShopAPI.Models
{
    public class OrderItemBindingModels
    {
        [Display(Name = "Наименование товара")]
        public string name { get; set; }
        [Display(Name = "Категория товара")]
        public string category { get; set; }
    }
}