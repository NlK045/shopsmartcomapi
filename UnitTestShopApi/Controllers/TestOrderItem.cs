using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ShopAPI.Controllers;
using ShopAPI.Database;
using ShopAPI.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace UnitTestShopApi
{
    [TestClass]
    public class TestOrderItem 
    {
        [TestMethod]
        public void GetAllOrderItems_ShouldReturnAllOrderItems()
        {
            var testOrderItem = GetTestOrderItem();
            var controller = new UnitTestOrderItemsController(testOrderItem);

            var result = controller.GetOrderItems() as List<OrderItem>;
            Assert.AreEqual(testOrderItem.Count, result.Count);
        }

        [TestMethod]
        public async Task GetAllOrderItemsAsync_ShouldReturnAllOrderItems()
        {
            var testOrderItem = GetTestOrderItem();
            var controller = new UnitTestOrderItemsController(testOrderItem);

            var result = await controller.GetAllOrderItems() as List<OrderItem>;
            Assert.AreEqual(testOrderItem.Count, result.Count);
        }

        [TestMethod]
        public void GetOrderItems_ShouldReturnCorrectOrderItems()
        {
            var testOrderItem = GetTestOrderItem();
            var controller = new UnitTestOrderItemsController(testOrderItem);

            var result = controller.GetOrderItems(4) as OkNegotiatedContentResult<OrderItem>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testOrderItem[3].name, result.Content.name);
        }

        [TestMethod]
        public async Task GetOrderItemsAsync_ShouldReturnCorrectOrderItems()
        {
            var testOrderItem = GetTestOrderItem();
            var controller = new UnitTestOrderItemsController(testOrderItem);

            var result = await controller.GetOrderItemsAsync(4) as OkNegotiatedContentResult<OrderItem>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testOrderItem[3].name, result.Content.name);
        }

        [TestMethod]
        public void GetOrderItems_ShouldNotFindOrderItems()
        {
            var controller = new UnitTestOrderItemsController(GetTestOrderItem());

            var result = controller.GetOrderItems(999);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        private List<OrderItem> GetTestOrderItem()
        {
            var testOrderItem = new List<OrderItem>();
            testOrderItem.Add(new OrderItem { id = 1, name = "������", category = "�����", price = 100 });
            testOrderItem.Add(new OrderItem { id = 2, name = "�������", category = "�����", price = 150 });
            testOrderItem.Add(new OrderItem { id = 3, name = "�����", category = "������", price = 200 });
            testOrderItem.Add(new OrderItem { id = 4, name = "������", category = "������", price = 75 });

            return testOrderItem;
        }
    }
}
